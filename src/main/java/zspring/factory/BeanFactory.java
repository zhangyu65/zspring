package zspring.factory;

import zspring.exception.XmlException;

public interface BeanFactory {
    Object getBean(String beanId) throws XmlException;
}
