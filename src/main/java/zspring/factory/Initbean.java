package zspring.factory;

import lombok.extern.slf4j.Slf4j;
import zspring.annotation.ZAutowired;
import zspring.constants.Constants;
import zspring.exception.XmlException;
import zspring.utils.AnnotationUtil;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class Initbean extends BeanDefinition {

    //初始化后的bean容器，key为class名，value为实例化对象
    public Map<String, Object> beanContainMap = new ConcurrentHashMap<>();

    /**
     * 初始化bean容器
     * 注意，扫描的bean会覆盖xml中配置的bean，spring也是这样，扫描的注入和装配都是在xml之后
     * ZAutowired是根据名称装配和扫描 TODO
     * @throws XmlException
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public void initBeans() throws XmlException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        //初始化xml配置
        initXmlBeans(Constants.contextConfigLoction);
        initXmlBeans(Constants.springmvcConfigLocation);
        initAutowireBeans(Constants.contextConfigLoction);
    }

    /**
     * 初始化注解的类
     * @param contextConfigLoction
     * @throws XmlException
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    private void initAutowireBeans(String contextConfigLoction) throws XmlException, ClassNotFoundException, InstantiationException, IllegalAccessException {
        List<String> componentList = super.getComponentList(contextConfigLoction);
        System.out.println("实例化的顺序" + componentList);
        //扫描到有注解的类，初始化类的名单
        for (String className : componentList){
            initClass(className);
        }
    }

    /**
     * 初始化每个类的方法
     * 需要根据类名来判断是否有接口，然后在将接口名和实现类对应上装配到容器中
     * @param className
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    private void initClass(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> clazz = Class.forName(className);
        Class<?>[] interfaces = clazz.getInterfaces();
        //如果类是接口，注入的对象是动态代理的对象
        if (clazz.isInterface()){
            //TODO
        }
        else if (interfaces == null || interfaces.length == 0){
            noInterfaceInit(className, className);
        }
        else {
            for (Class<?> interfaceClass : interfaces){
                //容器中如果有，则直接使用这个对象进行装配
                if (isExistInContainer(className)){
                    beanContainMap.put(interfaceClass.getName(), clazz.newInstance());
                } else {
                    //如果容器中没有，则先实例化实现类，然后再装配到容器中
                    noInterfaceInit(className, interfaceClass.getName());
                }
            }
        }
    }

    private boolean isExistInContainer(String className) {
        Set<Map.Entry<String, Object>> entries = beanContainMap.entrySet();
        if (entries != null) {
            for (Map.Entry<String, Object> entry : entries) {
                if (entry.getKey().equals(className)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private void noInterfaceInit(String className, String interfaceName) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> clazz = Class.forName(className);
        //bean实例化
        System.out.println("实例化的名字"+clazz.getName());
        Object object = clazz.newInstance();
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields){
            //如果属性上有ZAutowired注解，则先将属性注入进去
            if (!AnnotationUtil.isEmpty(field.getAnnotation(ZAutowired.class))){
                //设置私有属性可见
                field.setAccessible(true);
                //如果有注解，在实例化链表里面搜寻类名
                Set<Map.Entry<String, Object>> entries = beanContainMap.entrySet();
                for (Map.Entry<String, Object> entry : entries){
                    String type = field.getType().getName();
                    if (entry.getKey().equals(type)){
                        field.set(object, entry.getValue());
                    }
                }
            }
        }
        beanContainMap.put(interfaceName, object);
    }

    /**
     * 初始化xml中的bean
     * @param contextConfigLoction
     * @throws XmlException
     */
    public void initXmlBeans(String contextConfigLoction) throws XmlException {
        ApplicationContext applicationContext = new ApplicationContext(contextConfigLoction);
        Class<?> clazz = null;

        //从容器中取出bean，用application的getbean方法一次加载bean
        Map<String,GenericBeanDefinition> beanDefinitionMap = super.getBeanDefinitionXmlMap(contextConfigLoction);
        Set<Map.Entry<String,GenericBeanDefinition>> entries = beanDefinitionMap.entrySet();

        for (Map.Entry<String,GenericBeanDefinition> entry : entries){
            String beanId = entry.getKey();
            String className = entry.getValue().getClassName();

            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                log.error("xml中{}无法实例化", className);
                e.printStackTrace();
            }
            beanContainMap.put(className, clazz.cast(applicationContext.getBean(beanId)));
        }
    }
}
