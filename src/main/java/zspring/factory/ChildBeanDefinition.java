package zspring.factory;

import lombok.Data;

@Data
public class ChildBeanDefinition {

    private String childrenType;
    private String attributeOne;
    private String attributeTwo;
}
