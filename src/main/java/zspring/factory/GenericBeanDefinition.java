package zspring.factory;

import lombok.Data;

import java.util.List;

/**
 * 存放xml中的bean
 */
@Data
public class GenericBeanDefinition {


    private String className;

    /**
     * 存放 property 集合
     */
    private List<ChildBeanDefinition> childBeanDefinitionList;

}
