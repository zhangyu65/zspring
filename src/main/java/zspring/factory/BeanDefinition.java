package zspring.factory;

import lombok.extern.slf4j.Slf4j;
import zspring.exception.XmlException;
import zspring.xml.XmlApplicationContext;

import java.util.List;
import java.util.Map;

@Slf4j
public class BeanDefinition extends XmlApplicationContext{

    public List<String> getComponentList(String contextConfigLocation ) throws ClassNotFoundException, XmlException {
        List<String> componentList = super.getComponentList(contextConfigLocation);
        return componentList;
    }

    public  Map<String, GenericBeanDefinition> getBeanDefinitionXmlMap(String contextConfigLocation) throws XmlException {

        Map<String, GenericBeanDefinition> getbeanDefinitionXmlMap  = super.getBeanDefinitionMap(contextConfigLocation);

        return getbeanDefinitionXmlMap;
    }

}
