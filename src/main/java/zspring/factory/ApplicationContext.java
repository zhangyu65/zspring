package zspring.factory;

import jdk.management.resource.internal.inst.FileOutputStreamRMHooks;
import lombok.extern.slf4j.Slf4j;
import zspring.constants.Constants;
import zspring.exception.XmlException;
import zspring.utils.GetMethodName;
import zspring.xml.FileSystemXmlApplicationContext;
import zspring.xmlRules.IocRules;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Slf4j
public class ApplicationContext extends FileSystemXmlApplicationContext implements BeanFactory{

    public Map<String, GenericBeanDefinition> subMap = null;

    public ApplicationContext(String contextConfigLacation){
        this.subMap = super.getGenericBeanDefinition(contextConfigLacation);
    }

    @Override
    public Object getBean(String beanId) throws XmlException {

        assert beanId == null : "beanId不存在";
        Object object = null;
        Class<?> clazz = null;
        Set<Map.Entry<String, GenericBeanDefinition>> entries = subMap.entrySet();

        //判断容器中是否存在beanId
        if (subMap.containsKey(beanId)){
            //存在
            for (Map.Entry<String, GenericBeanDefinition> entry : entries){
                if (beanId.equals(entry.getKey())){
                    GenericBeanDefinition genericBeanDefinition = entry.getValue();
                    String beanName = genericBeanDefinition.getClassName();

                    //对象的属性集合
                    List<ChildBeanDefinition> childBeanDefinitionList = genericBeanDefinition.getChildBeanDefinitionList();

                    try {
                        clazz = Class.forName(beanName);
                    } catch (ClassNotFoundException e) {
                        log.error("{}没有找到", beanName);
                        e.printStackTrace();
                    }

                    try {
                        object = clazz.newInstance();
                    } catch (InstantiationException e) {
                        log.error("实例化对象异常");
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }

                    //遍历属性
                    for (ChildBeanDefinition childBeanDefinition : childBeanDefinitionList){
                        //set注入
                        if (IocRules.SET_INJECT.getType().equals(childBeanDefinition.getChildrenType())){
                            setValue(clazz, childBeanDefinition, object);
                        }
                        //构造器注入
                        else if (IocRules.CONS_INJECT.getType().equals(childBeanDefinition.getChildrenType())){
                            List<ChildBeanDefinition> constructorChildBeanDefinition = new ArrayList<>();

                            for (ChildBeanDefinition conChildBeanDefinition : childBeanDefinitionList){
                                if (IocRules.CONS_INJECT.getType().equals(conChildBeanDefinition.getChildrenType())){
                                    constructorChildBeanDefinition.add(conChildBeanDefinition);
                                }
                            }
                            object = consValue(clazz, constructorChildBeanDefinition, object);
                            break;
                        }
                    }
                }
            }
        } else {
            throw new XmlException("容器中不存在该bean");
        }
        return null;
    }

    private Object consValue(Class<?> clazz, List<ChildBeanDefinition> childBeanDefinitionList, Object origin) {
        Constructor<?> constructor = null;
        Field[] fields = clazz.getDeclaredFields();
        Class<?>[] classArray = new Class[fields.length];
        Object object = origin;
        for (int i=0; i<fields.length; i++){
            if ("String".equals(fields[i].getType().getSimpleName())){
                classArray[i] = String.class;
            } else if ("Integer".equals(fields[i].getType().getSimpleName())) {
                classArray[i] = Integer.class;
            } else if ("int".equals(fields[i].getType().getSimpleName())) {
                classArray[i] = int.class;
            }
        }

        try {
            constructor = clazz.getConstructor(classArray);

            //TODO
            try {
                object = constructor.newInstance(childBeanDefinitionList.get(0).getAttributeOne(),
                                                 childBeanDefinitionList.get(1).getAttributeOne());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }

        } catch (NoSuchMethodException e) {
            log.error("没有找到，{}", constructor);
        }
        return object;
    }

    private void setValue(Class<?> clazz, ChildBeanDefinition childBeanDefinition, Object object) {
        Field field = null;
        Method[] methods = null;

        String type = null;
        String propertyName = childBeanDefinition.getAttributeOne();
        String propertyValue = childBeanDefinition.getAttributeTwo();

        String methodName = GetMethodName.getSetMethodNameByField(propertyName);

        try {
            field = clazz.getDeclaredField(propertyName);
            type = field.getType().getSimpleName();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        try {
            methods = clazz.getMethods();
            for (Method method : methods) {
                if (methodName.equals(method.getName())) {
                    try {
                        if ("String".equals(type)) {
                            method.invoke(object, propertyValue);
                        } else if ("int".equals(type)) {
                            Integer propertyValue2 = Integer.valueOf(propertyValue);
                            method.invoke(object, propertyValue2);
                        } else if ("Integer".equals(type)) {
                            Integer propertyValue2 = Integer.valueOf(propertyValue);
                            method.invoke(object, propertyValue2);
                        } else {
                            log.error("暂时不支持该属性，{}", type);
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e){
            log.error("方法注入异常");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ApplicationContext(Constants.contextConfigLoction);

    }
}
