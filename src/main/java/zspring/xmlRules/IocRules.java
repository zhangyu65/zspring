package zspring.xmlRules;

import lombok.Getter;

@Getter
public enum IocRules {

    BEAN_RULE("bean", "id", "class"),
    SNAN_RULE("component-scan", "base-packge", "null"),

    /**
     * set注入规则
     */
    SET_INJECT("property", "name", "value"),

    /**
     * 构造器注入规则
     */
    CONS_INJECT("constructor-arg", "value", "index");


    private String type;
    private String name;
    private String value;

    IocRules(String type, String name, String value) {
        this.type = type;
        this.name = name;
        this.value = value;
    }
}
