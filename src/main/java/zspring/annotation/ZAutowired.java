package zspring.annotation;

import java.lang.annotation.*;

@Target({
    ElementType.CONSTRUCTOR, ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD, ElementType.ANNOTATION_TYPE
})
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ZAutowired {
    boolean required() default true;
}
