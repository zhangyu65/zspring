package zspring.constants;

import zspring.xml.FileSystemXmlApplicationContext;

public interface Constants {

    String PATH = FileSystemXmlApplicationContext.class.getResource("/").getPath();

    String contextConfigLoction = "application.xml";

    String springmvcConfigLocation = "spring-mvc.xml";

    String mybatisConfigLocation = "";
}
