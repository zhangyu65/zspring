package zspring.xml;

import lombok.extern.slf4j.Slf4j;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import zspring.constants.Constants;
import zspring.exception.XmlException;
import zspring.factory.ChildBeanDefinition;
import zspring.factory.GenericBeanDefinition;
import zspring.utils.ScanUtil;
import zspring.utils.StringUtil;
import zspring.xmlRules.IocRules;

import java.io.File;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 封装解析xml的方法，模仿ioc注入BeanDefinition,实际注入的是GenericBeanDefinition
 */
@Slf4j
public class XmlApplicationContext {

    /**
     * 将xml中的bean元素注入到容器中的方法
     * @param contextConfigLocation
     * @return beanDefinitionMap
     * @throws XmlException
     */
    public Map<String, GenericBeanDefinition> getBeanDefinitionMap(String contextConfigLocation) throws XmlException {
        Map<String, GenericBeanDefinition> beanDefinitionMap = new ConcurrentHashMap<>(256);
        List<Element> elementList = getElements(contextConfigLocation);
        for (Element element : elementList){
            if ("bean".equals(element.getName())){
                GenericBeanDefinition genericBeanDefinition = new GenericBeanDefinition();
                List<ChildBeanDefinition> childBeanDefinitionList = new ArrayList<>();
                String beanId = element.attributeValue(IocRules.BEAN_RULE.getName());
                String beanClass = element.attributeValue(IocRules.BEAN_RULE.getValue());

                if (!StringUtil.isEmpty(beanId) && !StringUtil.isEmpty(beanClass)){
                    genericBeanDefinition.setClassName(beanClass);
                    List<Element> elements = element.elements();
                    if (elements != null){
                        for (Element childrenElement : elements){
                            if (IocRules.SET_INJECT.getType().equals(childrenElement.getName())){
                                ChildBeanDefinition childBeanDefinition = new ChildBeanDefinition();
                                childBeanDefinition.setChildrenType(IocRules.SET_INJECT.getType());
                                String name = IocRules.SET_INJECT.getName();
                                String value = IocRules.SET_INJECT.getValue();
                                setChildBeanDefinitionByType(childrenElement, childBeanDefinition, name, value,childBeanDefinitionList);
                            }
                            else if (IocRules.CONS_INJECT.getType().equals(childrenElement.getName())){
                                ChildBeanDefinition childBeanDefinition = new ChildBeanDefinition();
                                childBeanDefinition.setChildrenType(IocRules.SET_INJECT.getType());
                                String name = IocRules.CONS_INJECT.getName();
                                String value = IocRules.CONS_INJECT.getValue();
                                setChildBeanDefinitionByType(childrenElement, childBeanDefinition, name, value,childBeanDefinitionList);
                            }
                        }
                    } else {
                        log.info("{}下面没有子元素", beanId);
                    }
                }
                genericBeanDefinition.setChildBeanDefinitionList(childBeanDefinitionList);
                beanDefinitionMap.put(beanId, genericBeanDefinition);
            }
        }
        return beanDefinitionMap;
    }

    /**
     * 根据指定的xml，获得注解扫描的bean容器
     * @param contextConfigLocation
     * @return componentList
     * @throws XmlException
     * @throws ClassNotFoundException
     */
    public List<String> getComponentList(String contextConfigLocation) throws XmlException, ClassNotFoundException {
        List<String> componentList = new ArrayList<>();
        List<Element> elementList = getElements(contextConfigLocation);
        for (Element element : elementList){
            String packageName = element.attributeValue(IocRules.SNAN_RULE.getName());
            componentList.addAll(resolveComponentList(packageName));
        }
        return componentList;
    }

    /**
     * 根据包名，返回扫描有注解的类
     * @param packageName
     * @return
     * @throws XmlException
     * @throws ClassNotFoundException
     */
    private Collection<String> resolveComponentList(String packageName) throws XmlException, ClassNotFoundException {
        if (StringUtil.isEmpty(packageName)){
            throw new XmlException("请正确设置" + IocRules.SNAN_RULE.getType() + "的属性");
        }
        List<String> componentList = new ArrayList<>();
        List<String> componentListAfter = ScanUtil.getComponentList(packageName);
        componentList.addAll(componentListAfter);
        return componentList;
    }

    /**
     * 注入bean子元素
     * @param childrenElement
     * @param childBeanDefinition
     * @param name
     * @param value
     * @param childBeanDefinitionList
     * @throws XmlException
     */
    private void setChildBeanDefinitionByType(Element childrenElement, ChildBeanDefinition childBeanDefinition, String name, String value, List<ChildBeanDefinition> childBeanDefinitionList) throws XmlException {
        if (childBeanDefinition != null){
            childBeanDefinition.setAttributeOne(childrenElement.attributeValue(name));
            childBeanDefinition.setAttributeTwo(childrenElement.attributeValue(value));
            childBeanDefinitionList.add(childBeanDefinition);
        } else {
            throw new XmlException("未按照格式配置xml文件或者暂不支持该配置属性");
        }
    }

    /**
     * 解析xml，获取素有元素
     * @param contextConfigLocation
     * @return
     */
    private List<Element> getElements(String contextConfigLocation) {

        SAXReader reader = new SAXReader();
        Document document = null;
        String pathName = Constants.PATH + contextConfigLocation;
        try {
            document = reader.read(new File(pathName));
        } catch (DocumentException e) {
            log.error("文件没有找到，{}", pathName);
        }
        Element node = document.getRootElement();
        List<Element> elementList = node.elements();
        return elementList;
    }
}
