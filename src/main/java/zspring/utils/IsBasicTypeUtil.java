package zspring.utils;

public class IsBasicTypeUtil {
    public static boolean isBasicType(String typeName){
        if (typeName.equals("String")){
            return true;
        }
        else if(typeName.equals("Integer")){
            return true;
        }
        else if(typeName.equals("int")){
            return true;
        }
        else if(typeName.equals("Long")){
            return true;
        }
        else if(typeName.equals("Short")){
            return true;
        }
        else if(typeName.equals("Float")){
            return true;
        }
        else if(typeName.equals("Double")){
            return true;
        }
        else if(typeName.equals("Byte")){
            return true;
        }
        return false;
    }
}
