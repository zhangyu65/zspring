package zspring.utils;

public class GetMethodName {
    public static String getSetMethodNameByField(String propertyName){
        String methodName = "set" + propertyName.substring(0,1).toUpperCase() + propertyName.substring(1);
        return methodName;
    }
}
