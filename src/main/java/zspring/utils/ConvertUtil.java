package zspring.utils;

public class ConvertUtil {


    public static Object  convert(String className,String parameter) {
        if (className.equals("String")) {
            return parameter;
        } else if (className.equals("Integer")) {
            return Integer.valueOf(parameter);
        } else if (className.equals("int")) {
            return Integer.valueOf(parameter);
        } else if (className.equals("Float")) {
            return Float.valueOf(parameter);
        } else if (className.equals("Double")) {
            return Integer.valueOf(parameter);
        } else if (className.equals("Long")) {
            return Long.valueOf(parameter);
        } else if (className.equals("Short")) {
            return Short.valueOf(parameter);
        } else if (className.equals("Byte")) {
            return Byte.valueOf(parameter);
        } else if (className.equals("Boolean")) {
            return Boolean.valueOf(parameter);
        }
        return null;
    }
}
