package zspring.utils;

import lombok.extern.slf4j.Slf4j;
import zspring.annotation.ZAutowired;
import zspring.annotation.ZController;
import zspring.annotation.ZRepository;
import zspring.annotation.ZService;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 1 扫描包下的注解
 * 2 扫描包下的类
 */
@Slf4j
public class ScanUtil {
    private static List<String> listClassName = new ArrayList<>();
    private static List<String> componentList = new ArrayList<>();
    private static Map<String,String> interfaceAndImplMap = new ConcurrentHashMap<>();

    /**
     * 扫描包下的所有类名
     * @param packageName
     * @return
     */
    public static List<String> getClassName(String packageName){
        Enumeration<URL> urls = null;
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        String newPackageName = packageName.replace(".", "/");
        try {
            urls = contextClassLoader.getResources(newPackageName);
            while (urls.hasMoreElements()){
                URL url = urls.nextElement();
                File packageFile = new File(url.getPath());
                File[] files = packageFile.listFiles();
                if (files == null){
                    break;
                }
                for (File file : files){
                    if(file.getName().endsWith(".class")){
                        String templeName = packageName.replace("/",".") + "." + file.getName();
                        String newTempleName = templeName.substring(0, templeName.lastIndexOf("."));
                        listClassName.add(newTempleName);
                    } else {
                        String nextPackageName = newPackageName + "." + file.getName();
                        getClassName(newPackageName);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listClassName;
    }

    /**
     * 获取所有实例
     * @param packageName
     * @return
     * @throws ClassNotFoundException
     */
    public static List<String> getComponentList(String packageName) throws ClassNotFoundException {
        List<String> classNameList = getClassName(packageName);
        //将扫描的接口和其实现类，使用map对应上,模仿spring接口注入，复杂的原因是java不支持从接口获取实现类
        makeInterfaceAndImplMap(classNameList);
        for (String className : classNameList){
            //实例化每个类
            resolveComponent(className);
        }
        return componentList;
    }

    private static void resolveComponent(String className) throws ClassNotFoundException {
        Class<?> clazz = Class.forName(className);
        //在此处添加要识别的注解，也是每次扫描的顺序，最好遵循习惯
        addNewAnnotation(ZController.class, clazz);
        addNewAnnotation(ZService.class, clazz);
        addNewAnnotation(ZRepository.class, clazz);
    }

    public static <A extends Annotation> void addNewAnnotation(Class<A> annotationClass, Class<?> clazz) throws ClassNotFoundException {
        //如果类上有注解，判断属性上有没有注解
        if (!AnnotationUtil.isEmpty(clazz.getAnnotation(annotationClass))){
            Field[] fields = clazz.getDeclaredFields();
            if (fields == null || fields.length == 0){
                ListAddUtil.add(componentList, clazz.getName());
            } else {
                //跳出递归的语句，也就是最底层的类，如果所有属性没有@MyAutowired注解，则注入到链表中
                if (isEmptyAutowired(fields)){
                    ListAddUtil.add(componentList, clazz.getName());
                } else {
                    for (Field field : fields){
                        //递归具体的查找到底哪个属性上有@zAutowired。
                        String newFieldName = field.getType().getName();
                        //如果是接口，则用其实现类注入
                        if (Class.forName(newFieldName).isInterface()){
                            String nextName = convertInterfaceImpl(newFieldName);
                            if (!componentList.contains(nextName)){
                                resolveComponent(nextName);
                            }
                        } else {
                            resolveComponent(newFieldName);
                        }
                    }
                }
                ListAddUtil.add(componentList, clazz.getName());
            }
        }
        else if (clazz.isInterface() && ("proxy").equals(interfaceAndImplMap.get(clazz.getName()))){
            ListAddUtil.add(componentList, clazz.getName());
        }
    }

    /**
     * 接口传转为实现类
     * @param newFieldName
     * @return
     */
    private static String convertInterfaceImpl(String newFieldName) {
        Set<Map.Entry<String, String>> entries = interfaceAndImplMap.entrySet();
        for (Map.Entry<String,String> entry : entries){
            if (newFieldName.equals(entry.getKey()) && !entry.getValue().equals("proxy")){
                return entry.getValue();
            } else if(newFieldName.equals(entry.getKey()) && entry.getValue().equals("proxy")) {
                return entry.getKey();
            }
        }
        return null;
    }

    private static boolean isEmptyAutowired(Field[] fields) {
        for (Field field : fields){
            if (!AnnotationUtil.isEmpty(field.getAnnotation(ZAutowired.class))){
                return false;
            }
        }
        return true;
    }

    /**
     * 组装接口和实现类
     * @param classNameList
     * @return
     */
    private static Map<String,String> makeInterfaceAndImplMap(List<String> classNameList) {
        Class<?> clazz = null;
        List<String> interfaceNameList = new ArrayList<>();
        List<String> interfaceExist = new ArrayList<>();

        for (String className : classNameList){
            try {
                clazz = Class.forName(className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            if (clazz.isInterface()){
                interfaceNameList.add(clazz.getName());
            }
        }

        for (String clazzName : classNameList){
            Class<?> aClass = null;
            try {
                aClass = Class.forName(clazzName);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            Class<?>[] interfaces = aClass.getInterfaces();
            if (interfaces != null && interfaces.length != 0){
                for (String interfaceName : interfaceNameList){
                    for (Class<?> interfaceClass : interfaces){
                        if (interfaceName.equals(interfaceClass.getName())){
                            interfaceAndImplMap.put(interfaceName, clazzName);
                            interfaceExist.add(interfaceName);
                        }
                    }
                }
            }
        }
        //需要动态代理注入的接口，在map中用value = proxy来识别
        interfaceNameList.removeAll(interfaceExist);
        if (interfaceNameList != null && interfaceNameList.size() > 0){
            for (String spareInterfaceName : interfaceNameList){
                interfaceAndImplMap.put(spareInterfaceName, "proxy");
            }
            System.out.println("已经存在的" + interfaceNameList);
        }
        return null;
    }
}
