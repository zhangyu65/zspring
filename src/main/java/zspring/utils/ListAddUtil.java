package zspring.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListAddUtil {

    public static <T> void add(List<T> list, T t){
        Set<T> set = new HashSet<>(list);
        if(set.add(t)){
            list.add(t);
        }
    }
}
